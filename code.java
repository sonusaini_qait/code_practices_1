import java.lang.*;
import java.util.*;

class Circle {
	
	 double radius ;
	 
	 double findArea() {
		 return radius * radius ;
	 }
	
	
}

class Rectangle {
	
	double length ;
	double breadth ;
	
	double findArea() {
		
		return length * breadth ; 
	}
	
}

class Square {
	
	double side ;
	
	double findArea() {
		
		return side * side ;
		
	}
	
	
}

class Triangle {
	
	double base ;
	double height ;
	
	double findArea() {
		
		return ( base * height )/2 ;
	}
	
}

public class Area {

	public static void main (String [] a) { 
		
		Scanner scan = new Scanner(System.in) ;
		
		String userInput ;
		
		
		double area  = 0 ;
		
		while(true) {
		
		System.out.println("Enter your choice : \n 1.CIRCLE \n 2.RECTANGLE \n 3.SQUARE \n 4.TRIANGLE \n 5.EXIT") ;

		userInput = scan.next() ;
		
		boolean isValid = true ;
		
		if(userInput.equals("CIRCLE")) {
			
			Circle circleObj = new Circle() ;
			
			System.out.println("Enter radius of Circle");
			
			circleObj.radius = scan.nextDouble() ;
			
			area = circleObj.findArea() ;
			
		} else if(userInput.equals("RECTANGLE")) {
			
			Rectangle rectangleObj = new Rectangle() ;
			
			System.out.println("Enter length of Rectangle");
			
			rectangleObj.length = scan.nextDouble() ;
			
			System.out.println("Enter breadth of Rectangle");
			
			rectangleObj.breadth = scan.nextDouble() ;
			
			area = rectangleObj.findArea() ;
			
		} else if(userInput.equals("SQUARE")) {
			userInput = scan.nextLine() ;
			Square squareObj = new Square() ;
	
			System.out.println("Enter side of Square");
			
			squareObj.side = scan.nextDouble() ;
			
			area = squareObj.findArea() ;
			
		} else if(userInput.equals("TRIANGLE")) {
			
			Triangle triangleObj = new Triangle() ;
			
			System.out.println("Enter base of Triangle");
			
			triangleObj.base = scan.nextDouble() ;
			
			System.out.println("Enter height of Triangle");
			
			triangleObj.height = scan.nextDouble() ;
			
			area = triangleObj.findArea() ;
			
			
		} else if(userInput.equals("EXIT")) {
			
			break;
			
		} else {
			
			System.out.println("Invalid Input ! please enter the correct shape");
			
			isValid = false ;
			
		}
		
		if( isValid ) {
		System.out.println("Area = " +  area) ;
		}
		
		} 
		 
		
		
		
	}
	
	
}
